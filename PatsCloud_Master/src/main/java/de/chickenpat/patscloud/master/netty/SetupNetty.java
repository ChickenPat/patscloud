package de.chickenpat.patscloud.master.netty;

import de.chickenpat.patscloud.core.util.Setup;
import de.chickenpat.patscloud.core.util.cachemanager.CacheManager;
import de.chickenpat.patscloud.core.util.command.CommandReader;
import de.chickenpat.patscloud.core.util.keymanager.KeyCreator;
import de.chickenpat.patscloud.core.util.netty.packets.handler.PacketDecoder;
import de.chickenpat.patscloud.core.util.netty.packets.handler.PacketEncoder;
import de.chickenpat.patscloud.core.util.netty.server.ServerNettyBootstrap;
import de.chickenpat.patscloud.master.PatsCloud_Master;
import de.chickenpat.patscloud.master.util.Settings;
import de.chickenpat.patscloud.master.util.netty.handler.ConnectionHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

import java.io.IOException;


public class SetupNetty extends Setup implements Settings {

    private static SetupNetty instance = new SetupNetty();
    private ServerNettyBootstrap nettyConnection;
    private CommandReader commandReader;


    public static SetupNetty getInstance() {
        return instance;
    }


    public ServerNettyBootstrap getNettyConnection() {
        return nettyConnection;
    }

    public CommandReader getCommandReader() {
        return commandReader;
    }

    @Override
    public void onLoad() {
    }

    public void onEnable() {

        registerNettyConnection();
        try {
            KeyCreator keyCreator = new KeyCreator("WrapperKey.key");
            PatsCloud_Master.getMaster().passwort = keyCreator.getPassword();
        } catch (Exception ignored) {
        }
        commandReader = new CommandReader(PatsCloud_Master.getMaster(), nettyConnection);
        try {
            commandReader.startReader();
        } catch (IOException e) {
            commandReader.close();
        }
    }

    @Override
    public void onDisable() {
    }

    private void registerNettyConnection() {
        CacheManager cacheManager = PatsCloud_Master.getMaster().getCacheManager();
        nettyConnection = new ServerNettyBootstrap(cacheManager.getInteger("Port"), cacheManager.getInteger("BackLog"), cacheManager.getBoolean("Epoll"), cacheManager.getBoolean("KeepAlive"), getChannelChannelInitializer());
    }

    private ChannelInitializer<SocketChannel> getChannelChannelInitializer() {
        return new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel channel) throws Exception {
                channel.pipeline().addLast(new PacketEncoder()).addLast(new PacketDecoder()).addLast(new ConnectionHandler());
            }
        };
    }

}
