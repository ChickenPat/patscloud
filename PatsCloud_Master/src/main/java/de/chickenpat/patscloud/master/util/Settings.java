package de.chickenpat.patscloud.master.util;

import de.chickenpat.patscloud.core.util.CoreSettings;

public interface Settings extends CoreSettings {
    String VERSION = "1.0.0";
    String SOFTWARE = "PatsCloud Manager";

}
