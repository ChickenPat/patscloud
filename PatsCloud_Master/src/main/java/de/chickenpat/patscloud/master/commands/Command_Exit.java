package de.chickenpat.patscloud.master.commands;

import de.chickenpat.patscloud.core.util.command.Command;
import de.chickenpat.patscloud.core.util.logmanager.LogManager;
import de.chickenpat.patscloud.core.util.netty.packets.message.exit.ExitPacket;
import de.chickenpat.patscloud.master.PatsCloud_Master;
import de.chickenpat.patscloud.master.netty.SetupNetty;
import io.netty.channel.Channel;

import java.util.Random;

public class Command_Exit implements Command {
    @Override
    public boolean called(String[] args, Channel channel) {
        return false;
    }

    @Override
    public void action(String[] args, Channel channel) {

        Random random = new Random();
        LogManager.getInstance().sendMessage("Master is shutting down...");
        PatsCloud_Master.getMaster().sleep(random.nextInt(300) + 200);
        SetupNetty.getInstance().getNettyConnection().close();
        LogManager.getInstance().sendMessage("Netty master has been closed");
        PatsCloud_Master.getMaster().sleep(random.nextInt(300) + 200);
        SetupNetty.getInstance().getCommandReader().close();
        LogManager.getInstance().sendMessage("Command Reader has been closed");
        PatsCloud_Master.getMaster().sleep(random.nextInt(300) + 200);
        LogManager.getInstance().sendMessage("Server shuts down successfully. Bye");
    }

    @Override
    public void executed(boolean success, Channel channel) {

    }
}
