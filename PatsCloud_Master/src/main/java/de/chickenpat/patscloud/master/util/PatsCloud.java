package de.chickenpat.patscloud.master.util;

import de.chickenpat.patscloud.core.util.Cloud;
import de.chickenpat.patscloud.core.util.filemanager.FileManager;
import de.chickenpat.patscloud.core.util.json.JSONConfiguration;
import de.chickenpat.patscloud.core.util.types.wrapper.Wrapper;
import de.chickenpat.patscloud.master.PatsCloud_Master;
import de.chickenpat.patscloud.master.commands.Command_Exit;
import io.netty.channel.Channel;
import io.netty.channel.epoll.Epoll;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

public class
PatsCloud extends Cloud {
    public void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void registerCommands() {
        registerCommand("exit", new Command_Exit());
    }


    public File getDataFolder() {
        File file = new File("PatsCloud-Master");
        if (!file.exists()) {
            file.mkdir();
        }
        return file;
    }

    @Override
    public void loadConfigFiles() {
        try {
            JSONConfiguration jsonConfiguration = new JSONConfiguration(new File(getDataFolder(),"config.json"));
            getCacheManager().put("Port",jsonConfiguration.getJsonObject().get("cloudPort"));
            getCacheManager().put("Epoll",((String)jsonConfiguration.getJsonObject().get("epoll")).equalsIgnoreCase("auto") ? Epoll.isAvailable() : jsonConfiguration.getJsonObject().get("epoll"));
            getCacheManager().put("KeepAlive",jsonConfiguration.getJsonObject().get("keepAlive"));
            getCacheManager().put("BackLog",jsonConfiguration.getJsonObject().get("backLog"));
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initResourceFiles() {
        FileManager fileManager = PatsCloud_Master.getMaster().getFileManager();
        fileManager.getResourceFileLoader().registerResourceFile("config.json", "default", new HashMap<>(),null);
        fileManager.getResourceFileLoader().registerResourceFile("wrappers.json", "default", new HashMap<>(),null);
        try {
            fileManager.getResourceFileLoader().loadDir();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
