package de.chickenpat.patscloud.master;


import de.chickenpat.patscloud.core.util.filemanager.FileManager;
import de.chickenpat.patscloud.core.util.logmanager.LogManager;
import de.chickenpat.patscloud.core.util.logmanager.LogType;
import de.chickenpat.patscloud.master.netty.SetupNetty;
import de.chickenpat.patscloud.master.util.PatsCloud;
import de.chickenpat.patscloud.master.util.Settings;

public class PatsCloud_Master extends PatsCloud implements Settings {

    private static PatsCloud_Master master = new PatsCloud_Master();
    private FileManager fileManager;

    public static PatsCloud_Master getMaster() {
        return master;
    }

    public static void main(String[] args) {
        //TEMP
        LogManager.getInstance().setDebug(true);
        //END:TEMP

        master.sendStartUpMessage();
        master.startUp();
    }

    public FileManager getFileManager() {
        return fileManager;
    }

    private void startUp() {

        LogManager.getInstance().sendMessage(LogType.INFO, "Initialize Default Files...");
        fileManager = new FileManager(PatsCloud_Master.getMaster());
        fileManager.loadResourceFiles();
        LogManager.getInstance().sendMessage(LogType.INFO, "Load Configuration...");
        PatsCloud_Master.getMaster().loadConfigFiles();
        LogManager.getInstance().sendMessage(LogType.INFO, "Initialize Commands...");
        PatsCloud_Master.getMaster().registerCommands();
        SetupNetty.getInstance().onEnable();

    }


    private void sendStartUpMessage() {
        System.out.println(
                "   ______       _          _____  _                    _ \n" +
                        "   | ___ \\     | |        /  __ \\| |                  | |\n" +
                        "   | |_/ /__ _ | |_  ___  | /  \\/| |  ___   _   _   __| |\n" +
                        "   |  __// _` || __|/ __| | |    | | / _ \\ | | | | / _` |\n" +
                        "   | |  | (_| || |_ \\__ \\ | \\__/\\| || (_) || |_| || (_| |\n" +
                        "   \\_|   \\__,_| \\__||___/  \\____/|_| \\___/  \\__,_| \\__,_|\n" +
                        "   Software: " + SOFTWARE + "    |   Version: " + VERSION + "\n");

    }


}
