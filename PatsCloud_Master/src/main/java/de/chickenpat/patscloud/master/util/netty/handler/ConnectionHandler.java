package de.chickenpat.patscloud.master.util.netty.handler;

import de.chickenpat.patscloud.core.util.CloudHelper;
import de.chickenpat.patscloud.core.util.logmanager.LogManager;
import de.chickenpat.patscloud.core.util.logmanager.LogType;
import de.chickenpat.patscloud.core.util.netty.packets.Packet;
import de.chickenpat.patscloud.core.util.netty.packets.message.information.VerificationInformationPacket;
import de.chickenpat.patscloud.core.util.netty.packets.message.information.VerificationInformationRequestPacket;
import de.chickenpat.patscloud.core.util.netty.packets.message.password.PasswordVerificationPacket;
import de.chickenpat.patscloud.core.util.types.Connection;
import de.chickenpat.patscloud.core.util.types.wrapper.Wrapper;
import de.chickenpat.patscloud.master.PatsCloud_Master;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.HashMap;

public class ConnectionHandler extends SimpleChannelInboundHandler<Packet> {

    private Channel channel;
    private HashMap<Integer, String> passwordHashMap = new HashMap<>();
    public HashMap<Channel, Connection> wrappers = PatsCloud_Master.getMaster().registerConnection;

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        LogManager.getInstance().sendMessage("Wrapper Client disconnect » " + channel.remoteAddress());
    }


    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        channel = ctx.channel();
        LogManager.getInstance().sendMessage("Wrapper Client connect » " + channel.remoteAddress());

    }

    @Override
    protected void messageReceived(ChannelHandlerContext ctx, Packet packet) throws Exception {
        if (packet instanceof PasswordVerificationPacket) {
            PasswordVerificationPacket passwordVerificationPacket = (PasswordVerificationPacket) packet;
            passwordHashMap.put(passwordVerificationPacket.getPasswordPart(), passwordVerificationPacket.getPassword());
            if (passwordVerificationPacket.getPassword().equals("--FINISH--") && passwordVerificationPacket.getPasswordPart() == -1) {
                if (CloudHelper.getInstance().composePassword(passwordHashMap).equals(PatsCloud_Master.getMaster().passwort)) {
                    wrappers.put(getChannel(),null);
                    channel.writeAndFlush(new VerificationInformationRequestPacket());
                    LogManager.getInstance().sendMessage("Wrapper verified » " + channel.remoteAddress());
                }
            }
            return;
        }
        if (!wrappers.containsKey(getChannel())) {
            LogManager.getInstance().sendDebugMessage(LogType.ERROR, "Deny Packet » Identification: " + CloudHelper.getInstance().getIDViaPacket(packet.getClass()) + " Name: " + packet.getClass().getSimpleName());
        }
        if (packet instanceof VerificationInformationPacket) {
            VerificationInformationPacket informationPacket = (VerificationInformationPacket) packet;
            wrappers.put(getChannel(),new Wrapper(informationPacket.getWrapperId(),informationPacket.getInternalIp(),informationPacket.getRam(),informationPacket.getStartPort(),informationPacket.getStartPort()+informationPacket.getBackLog()));
        }
    }

    public Channel getChannel() {
        return channel;
    }

}
