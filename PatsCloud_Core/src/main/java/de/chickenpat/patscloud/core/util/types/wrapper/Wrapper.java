package de.chickenpat.patscloud.core.util.types.wrapper;

import de.chickenpat.patscloud.core.util.types.Connection;

import java.util.ArrayList;

public class Wrapper extends Connection {

    private Integer startPort, endPort;
    private ArrayList<Template> templates = new ArrayList<>();
    private ArrayList<Server> servers = new ArrayList<>();
    private ArrayList<Port> ports = new ArrayList<>();


    public Wrapper(String name, String address, Integer ram, Integer startPort, Integer endPort) {
        this.name = name;
        this.address = address;
        this.ram = ram;
        this.startPort = startPort;
        this.endPort = endPort;
        Integer i = startPort;
        while (i <= endPort) {
            addPort(new Port(i, false));
            i++;
        }
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getRam() {
        return ram;
    }

    public void setRam(Integer ram) {
        this.ram = ram;
    }

    public Integer getStartPort() {
        return startPort;
    }

    public void setStartPort(Integer startPort) {
        this.startPort = startPort;
    }

    public Integer getEndPort() {
        return endPort;
    }

    public void setEndPort(Integer stopPort) {
        this.endPort = stopPort;
    }

    public Integer getUsedRam() {
        return usedRam;
    }

    public void setUsedRam(Integer ramUsed) {
        this.usedRam = ramUsed;
    }

    public ArrayList<Template> getTemplates() {
        return templates;
    }

    public void setTemplates(ArrayList<Template> templates) {
        this.templates = templates;
    }

    public ArrayList<Server> getServers() {
        return servers;
    }

    public void setServers(ArrayList<Server> servers) {
        this.servers = servers;
    }

    public ArrayList<Port> getPorts() {
        return ports;
    }

    public void setPorts(ArrayList<Port> ports) {
        this.ports = ports;
    }

    public void addServer(Server server) {
        servers.add(server);
    }

    public void removeServer(Server server) {
        servers.remove(server);
    }

    public void addTemplate(Template template) {
        templates.add(template);
    }

    public void removeTemplate(Template template) {
        templates.remove(template);
    }

    public void addPort(Port port) {
        ports.add(port);
    }

    public void removePort(Port port) {
        ports.remove(port);
    }
}
