/*
 * Copyright (c) 2018 by ChickenPat.
 */

package de.chickenpat.patscloud.core.util.filemanager;

import java.io.InputStream;
import java.util.HashMap;

public class ResourceFile {

    private InputStream inputStream;
    private HashMap<String, String> replace;
    private ResourceCreateAction action;

    public ResourceFile(InputStream inputStream, HashMap<String, String> replace, ResourceCreateAction action){
        this.replace = replace;
        this.inputStream = inputStream;
        this.action = action;
    }

    public HashMap<String, String> getReplace() {
        return replace;
    }

    public void setReplace(HashMap<String, String> replace) {
        this.replace = replace;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public ResourceCreateAction getAction() {
        return action;
    }

    public void setAction(ResourceCreateAction action) {
        this.action = action;
    }
}
