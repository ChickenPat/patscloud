package de.chickenpat.patscloud.core.util.command;

import io.netty.channel.Channel;

public interface Command {
    boolean called(String[] args, Channel channel);

    void action(String[] args, Channel channel);

    void executed(boolean success, Channel channel);

}
