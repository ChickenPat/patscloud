/*
 * Copyright (c) 2018 by ChickenPat.
 */

package de.chickenpat.patscloud.core.util.filemanager;

public abstract class ResourceCreateAction {
    public abstract void onPreCreation(ResourceFile file);
    public abstract void onCreation(ResourceFile file);

}
