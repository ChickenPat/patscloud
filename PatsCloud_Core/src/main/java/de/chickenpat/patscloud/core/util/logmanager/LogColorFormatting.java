package de.chickenpat.patscloud.core.util.logmanager;

public enum LogColorFormatting {
    NONE("0"),
    BOLD("1"),
    ITALICS("3"),
    ITALICS_BOLD("3;1"),
    UNDERLINE("4"),
    UNDERLINE_BOLD("4;1"),
    UNDERLINE_ITALICS("4;3"),
    UNDERLINE_ITALICS_BOLD("4;3;1");

    private String var;

    LogColorFormatting(String var) {
        this.var = var;
    }

    public String getVar() {
        return var;
    }
}
