package de.chickenpat.patscloud.core.util.command.parser;

import io.netty.channel.Channel;

import java.util.ArrayList;
import java.util.Arrays;

public class commandParser {
    public commandContainer parse(String raw, Channel channel) {
        String beheaded = raw.replaceFirst("", "");
        String[] splitBeheaded = beheaded.split(" ");
        String invoke = splitBeheaded[0];
        ArrayList<String> split = new ArrayList<String>(Arrays.asList(splitBeheaded));
        String[] args = new String[split.size() - 1];
        split.subList(1, split.size()).toArray(args);

        return new commandContainer(raw, beheaded, splitBeheaded, invoke, args, channel);
    }

    public class commandContainer {

        final String raw;
        final String beheaded;
        final String[] splitBeheaded;
        public final String invoke;
        public final String[] args;
        public final Channel channel;

        commandContainer(String rw, String beheaded, String[] splitBeheaded, String invoke, String[] args, Channel channel) {
            this.raw = rw;
            this.beheaded = beheaded;
            this.splitBeheaded = splitBeheaded;
            this.invoke = invoke;
            this.args = args;
            this.channel = channel;
        }

    }
}
