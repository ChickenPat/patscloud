/*
 * Copyright (c) 2018 by ChickenPat.
 */

package de.chickenpat.patscloud.core.util.keymanager;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class KeyLoader {

    private String raw, password, ip;
    private Integer port;
    public KeyLoader(String s){
        raw = s;
        password = s.substring(0,9600);
        ip = hexToAscii(s.substring(9600).split("3b")[0]);
        port = Integer.valueOf(hexToAscii(s.substring(9600).split("3b")[1]));
    }

    public KeyLoader(File file) throws IOException {
        this(FileUtils.readFileToString(file));
    }

    private String hexToAscii(String hexStr) {
        StringBuilder output = new StringBuilder();

        for (int i = 0; i < hexStr.length(); i += 2) {
            String str = hexStr.substring(i, i + 2);
            output.append((char) Integer.parseInt(str, 16));
        }

        return output.toString();
    }

    public String getRaw() {
        return raw;
    }

    public void setRaw(String raw) {
        this.raw = raw;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
}
