package de.chickenpat.patscloud.core.util.logmanager;


import de.chickenpat.patscloud.core.util.CloudHelper;

public class LogManager {

    private static LogManager logManager = new LogManager();

    public static LogManager getInstance() {
        return logManager;
    }

    private boolean debug = false;

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public boolean isDebug() {
        return debug;
    }

    public void sendDebugMessage(LogType type, String message) {
        if(isDebug()) System.out.println(type.getPrefixColor() + "[" + CloudHelper.getInstance().getCurrentTimeOfDay() + "/" + type.getPrefix() + "] " +LogColorType.CYAN+"DEBUG"+LogColorType.RESET+" × "+ type.getColor() + message + LogColorType.RESET);
    }

    public void sendMessage(LogType type, String message) {
        System.out.println(type.getPrefixColor() + "[" + CloudHelper.getInstance().getCurrentTimeOfDay() + "/" + type.getPrefix() + "]" + type.getColor() + " " + message + LogColorType.RESET);
    }

    public void sendDebugMessage(String message) {
        sendDebugMessage(LogType.INFO, message);
    }

    public void sendMessage(String message) {
        sendMessage(LogType.INFO, message);
    }

    public String translateColorCodes(char code, String text) {
        StringBuilder output = new StringBuilder();
        boolean found = false;
        boolean activeBackground = false, activeHigh = false;
        for (int j = 0; j < text.toCharArray().length; j++) {
            if (found) {
                found = false;
                continue;
            }
            String s = String.valueOf(text.toCharArray()[j]) + (text.toCharArray().length - 1 > j ? text.toCharArray()[j + 1] : "");
            if (s.equals(code + "r")) {
                activeHigh = false;
                activeBackground = false;
                output.append("\033[0m");
                found = true;
                continue;
            }
            if (s.equals(code + "b")) {
                output.append("\033[1m");
                found = true;
                continue;
            }
            if (s.equals(code + "k")) {
                output.append("\033[3m");
                found = true;
                continue;
            }
            if (s.equals(code + "h")) {
                activeHigh = true;
                found = true;
                continue;
            }
            if (s.equals(code + "u")) {
                output.append("\033[4m");
                found = true;
                continue;
            }
            if (s.equals(code + "g")) {
                activeBackground = true;
                found = true;
                continue;
            }
            for (int i = 0; i <= LogColorType.values().length - 1; i++) {
                LogColorType colorType = LogColorType.values()[i];
                if (s.equals(String.valueOf(code) + i)) {
                    output.append(new LogColor(colorType).setHighintensity(activeHigh).setBackground(activeBackground));
                    found = true;
                }
            }
            if (!found) {
                output.append(text.toCharArray()[j]);
            }
        }
        return output.toString();
    }
}
