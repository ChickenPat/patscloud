package de.chickenpat.patscloud.core.util.command.handler;

import de.chickenpat.patscloud.core.util.Cloud;
import de.chickenpat.patscloud.core.util.command.parser.commandParser;

public class commandHandler {

    public static final commandParser parser = new commandParser();

    public static void handleCommand(Cloud cloud, commandParser.commandContainer cmd) {
        if (cloud.commands.containsKey(cmd.invoke.toLowerCase())) {

            boolean safe = cloud.commands.get(cmd.invoke.toLowerCase()).called(cmd.args, cmd.channel);
            if (!safe) {
                cloud.commands.get(cmd.invoke.toLowerCase()).action(cmd.args, cmd.channel);
                cloud.commands.get(cmd.invoke.toLowerCase()).executed(false, cmd.channel);
            } else {
                cloud.commands.get(cmd.invoke.toLowerCase()).executed(true, cmd.channel);

            }
        }
    }
}
