package de.chickenpat.patscloud.core.util.types.wrapper;

public class Port {

    private Integer port;
    private boolean used;

    public Port(Integer port, boolean used) {
        this.port = port;
        this.used = used;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public boolean isUsed() {
        return used;
    }
}
