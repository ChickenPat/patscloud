package de.chickenpat.patscloud.core.util;

import de.chickenpat.patscloud.core.util.cachemanager.CacheManager;
import de.chickenpat.patscloud.core.util.command.Command;
import de.chickenpat.patscloud.core.util.logmanager.LogManager;
import de.chickenpat.patscloud.core.util.types.Connection;
import io.netty.channel.Channel;

import java.io.File;
import java.util.HashMap;

public abstract class Cloud {
    public HashMap<String, Command> commands = new HashMap<>();
    private CacheManager cacheManager = new CacheManager();
    public String passwort;
    public HashMap<Channel, Connection> registerConnection = new HashMap<>();

    public CacheManager getCacheManager() {
        return cacheManager;
    }

    public abstract File getDataFolder();

    public abstract void loadConfigFiles();

    public abstract void initResourceFiles();

    public abstract void registerCommands();

    public void registerCommand(String name, Command command) {
        commands.put(name.toLowerCase(), command);
        LogManager.getInstance().sendMessage("Register Command: " + name);
    }

}
