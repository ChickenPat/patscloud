/*
 * Copyright (c) 2018 by ChickenPat.
 */

package de.chickenpat.patscloud.core.util.netty.packets.message.information;

import de.chickenpat.patscloud.core.util.netty.packets.Packet;
import de.chickenpat.patscloud.core.util.netty.packets.buffer.PatsBuffer;

public class VerificationInformationPacket extends Packet  {

    private String wrapperId, internalIp;
    private Integer ram, startPort, backLog;

    public VerificationInformationPacket() {
    }

    public VerificationInformationPacket(String wrapperId, String internalIp, Integer ram, Integer startPort, Integer backLog){
        this.wrapperId = wrapperId;
        this.internalIp = internalIp;
        this.ram = ram;
        this.startPort = startPort;
        this.backLog = backLog;
    }

    @Override
    public void write(PatsBuffer byteBuf) {
        byteBuf.writeString(wrapperId);
        byteBuf.writeString(internalIp);
        byteBuf.writeVariableInt(ram);
        byteBuf.writeVariableInt(startPort);
        byteBuf.writeVariableInt(backLog);
    }

    @Override
    public void read(PatsBuffer byteBuf) {
        wrapperId = byteBuf.readString();
        internalIp = byteBuf.readString();
        ram = byteBuf.readVariableInt();
        startPort = byteBuf.readVariableInt();
        backLog = byteBuf.readVariableInt();
    }

    public String getWrapperId() {
        return wrapperId;
    }

    public String getInternalIp() {
        return internalIp;
    }

    public Integer getRam() {
        return ram;
    }

    public Integer getBackLog() {
        return backLog;
    }

    public Integer getStartPort() {
        return startPort;
    }
}