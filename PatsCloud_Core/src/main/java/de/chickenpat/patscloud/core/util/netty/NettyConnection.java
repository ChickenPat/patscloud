package de.chickenpat.patscloud.core.util.netty;

import io.netty.channel.Channel;

public interface NettyConnection {

    void doConnect();

    void close();

    Channel getChannel();
}
