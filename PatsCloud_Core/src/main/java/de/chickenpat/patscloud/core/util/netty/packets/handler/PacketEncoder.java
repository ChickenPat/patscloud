package de.chickenpat.patscloud.core.util.netty.packets.handler;

import de.chickenpat.patscloud.core.util.CloudHelper;
import de.chickenpat.patscloud.core.util.logmanager.LogManager;
import de.chickenpat.patscloud.core.util.netty.packets.Packet;
import de.chickenpat.patscloud.core.util.netty.packets.buffer.PatsBuffer;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class PacketEncoder extends MessageToByteEncoder<Packet> {




    protected void encode(ChannelHandlerContext ctx, Packet packet, ByteBuf output) throws Exception {
        PatsBuffer patsBuffer = new PatsBuffer(output);
        if (!ctx.channel().isOpen())
            return;
        int id = CloudHelper.getInstance().getIDViaPacket(packet.getClass());
        if (id < 1)
            throw new NullPointerException("Can't find id of packet " + packet.getClass().getSimpleName());

        patsBuffer.writeVariableInt(id);
        packet.write(patsBuffer);
        LogManager.getInstance().sendDebugMessage("Send Packet » Identification: " + CloudHelper.getInstance().getIDViaPacket(packet.getClass()) + " Name: " + packet.getClass().getSimpleName());
    }

}
