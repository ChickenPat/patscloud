/*
 * Copyright (c) 2018 by ChickenPat.
 */

package de.chickenpat.patscloud.core.util.netty.packets;

import de.chickenpat.patscloud.core.util.netty.packets.message.information.VerificationInformationPacket;
import de.chickenpat.patscloud.core.util.netty.packets.message.information.VerificationInformationRequestPacket;
import de.chickenpat.patscloud.core.util.netty.packets.message.password.PasswordDenyPacket;
import de.chickenpat.patscloud.core.util.netty.packets.message.password.PasswordVerificationPacket;

public enum Protocol {

    PASSWORD_VERIFICATION(1, PasswordVerificationPacket.class),
    PASSWORD_DENY(2, PasswordDenyPacket.class),
    INFORMATION_REQUEST(3, VerificationInformationRequestPacket.class),
    INFORMATION(4, VerificationInformationPacket.class);

    private int packetID;
    private Class<? extends Packet> packetClass;

    Protocol(int packetID, Class<? extends Packet> packetClass) {
        this.packetID = packetID;
        this.packetClass = packetClass;
    }


    public Class<? extends Packet> getPacketClass() {
        return packetClass;
    }

    public int getPacketID() {
        return packetID;
    }
}
