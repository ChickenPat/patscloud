package de.chickenpat.patscloud.core.util.command;

import de.chickenpat.patscloud.core.util.Cloud;
import de.chickenpat.patscloud.core.util.command.handler.commandHandler;
import de.chickenpat.patscloud.core.util.netty.NettyConnection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CommandReader {

    private BufferedReader bufferedReader;
    private InputStreamReader inputStreamReader;
    private Cloud cloud;
    private NettyConnection nettyConnection;

    public CommandReader(Cloud cloud, NettyConnection nettyConnection) {
        this.cloud = cloud;
        this.nettyConnection = nettyConnection;
        inputStreamReader = new InputStreamReader(System.in);
        bufferedReader = new BufferedReader(inputStreamReader);
    }

    public void startReader() throws IOException {
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            if (line.equals(""))
                continue;
            commandHandler.handleCommand(cloud, commandHandler.parser.parse(line, nettyConnection.getChannel()));
        }
        close();
    }

    public void close() {
        try {
            bufferedReader.close();
            inputStreamReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
