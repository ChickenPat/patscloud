package de.chickenpat.patscloud.core.util.filemanager;


import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class ResourceFileLoader {

    private File dataFolder;
    private HashMap<String, ResourceFile> resourceFiles;

    public ResourceFileLoader(HashMap<String, ResourceFile> resourceFiles, File dataFolder) {
        this.resourceFiles = resourceFiles;
        this.dataFolder = dataFolder;
    }

    public File getDataFolder() {
        return dataFolder;
    }

    private InputStream getResourceStream(String path) {
        return getClass().getClassLoader().getResourceAsStream(path);
    }

    public HashMap<String, String> createReplaceHashMap(ReplaceItem... items){
        HashMap<String, String> hashMap = new HashMap<>();
        for(ReplaceItem item : items){
            hashMap.put(item.getVar(),item.getVar2());
        }
        return hashMap;
    }

    public void registerResourceFile(String path, String defaultpath, HashMap<String, String> replace, ResourceCreateAction action) {
        resourceFiles.put(path.replace("/", System.getProperty("file.separator")), new ResourceFile(getResourceStream(defaultpath + "/" + path),replace,action));
    }

    public void loadDir() throws IOException {
        for (String path : resourceFiles.keySet()) {
            InputStream inputStream;
            inputStream = resourceFiles.get(path).getInputStream();
            File dir = new File(dataFolder + System.getProperty("file.separator") + (path.contains(System.getProperty("file.separator")) ? path.substring(0, path.lastIndexOf(System.getProperty("file.separator"))) : ""));
            if (!dir.exists()) {
                dir.mkdirs();
            }
            File file = new File(dataFolder + System.getProperty("file.separator") + path);
            if (!file.exists()) {
                ResourceCreateAction action = resourceFiles.get(path).getAction();
                    Objects.requireNonNull(action).onPreCreation(resourceFiles.get(path));
                OutputStream os = new FileOutputStream(dataFolder + System.getProperty("file.separator") + path);
                org.apache.commons.io.IOUtils.copy(inputStream, os);
                os.close();
                if(!resourceFiles.get(path).getReplace().isEmpty()){
                    ArrayList<String> content = new ArrayList<>();
                    FileReader fr = new FileReader(file);
                    BufferedReader br = new BufferedReader(fr);
                    String line;
                    while((line = br.readLine()) != null){
                        content.add(replaceContent(line,resourceFiles.get(path).getReplace()));
                    }
                    br.close();
                    PrintWriter writer = new PrintWriter(new FileWriter(file,false));
                    for(String s : content) {
                        writer.println(s);
                    }
                    writer.close();
                }
                Objects.requireNonNull(action).onCreation(resourceFiles.get(path));
            }
        }
    }

    private String replaceContent(String str, HashMap<String,String> replace){
        String newString = str;
        for(String s : replace.keySet()){
            newString = newString.replace(s,replace.get(s));
        }
        return newString;
    }

}
