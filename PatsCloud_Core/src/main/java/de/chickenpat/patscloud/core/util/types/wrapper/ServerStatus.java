package de.chickenpat.patscloud.core.util.types.wrapper;

public enum ServerStatus {

    ONLINE,
    OFFLINE,
    LOBBY,
    INGAME,
    MAINTENANCE,


}
