package de.chickenpat.patscloud.core.util;

public abstract class Setup {

    public abstract void onLoad();

    public abstract void onEnable();

    public abstract void onDisable();

}
