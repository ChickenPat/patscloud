/*
 * Copyright (c) 2018 by ChickenPat.
 */

package de.chickenpat.patscloud.core.util.netty.packets.buffer;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.ByteBufProcessor;
import io.netty.handler.codec.DecoderException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.GatheringByteChannel;
import java.nio.channels.ScatteringByteChannel;
import java.nio.charset.Charset;
import java.util.UUID;

public class PatsBuffer extends ByteBuf {

    private final ByteBuf buf;

    public PatsBuffer(ByteBuf buf) {
        this.buf = buf;
    }

    @Override
    public int capacity() {
        return this.buf.capacity();
    }

    @Override
    public ByteBuf capacity(int i) {
        return this.buf.capacity(i);
    }

    @Override
    public int maxCapacity() {
        return this.buf.maxCapacity();
    }

    @Override
    public ByteBufAllocator alloc() {
        return this.buf.alloc();
    }

    @Override
    @SuppressWarnings("deprecation")
    public ByteOrder order() {
        return this.buf.order();
    }

    @Override
    @SuppressWarnings("deprecation")
    public ByteBuf order(ByteOrder byteorder) {
        return this.buf.order(byteorder);
    }

    @Override
    public ByteBuf unwrap() {
        return this.buf.unwrap();
    }

    @Override
    public boolean isDirect() {
        return this.buf.isDirect();
    }

    @Override
    public int readerIndex() {
        return this.buf.readerIndex();
    }

    @Override
    public ByteBuf readerIndex(int i) {
        return this.buf.readerIndex(i);
    }

    @Override
    public int writerIndex() {
        return this.buf.writerIndex();
    }

    @Override
    public ByteBuf writerIndex(int i) {
        return this.buf.writerIndex(i);
    }

    @Override
    public ByteBuf setIndex(int i, int j) {
        return this.buf.setIndex(i, j);
    }

    @Override
    public int readableBytes() {
        return this.buf.readableBytes();
    }

    @Override
    public int writableBytes() {
        return this.buf.writableBytes();
    }

    @Override
    public int maxWritableBytes() {
        return this.buf.maxWritableBytes();
    }

    @Override
    public boolean isReadable() {
        return this.buf.isReadable();
    }

    @Override
    public boolean isReadable(int i) {
        return this.buf.isReadable(i);
    }

    @Override
    public boolean isWritable() {
        return this.buf.isWritable();
    }

    @Override
    public boolean isWritable(int i) {
        return this.buf.isWritable(i);
    }

    @Override
    public ByteBuf clear() {
        return this.buf.clear();
    }

    @Override
    public ByteBuf markReaderIndex() {
        return this.buf.markReaderIndex();
    }

    @Override
    public ByteBuf resetReaderIndex() {
        return this.buf.resetReaderIndex();
    }

    @Override
    public ByteBuf markWriterIndex() {
        return this.buf.markWriterIndex();
    }

    @Override
    public ByteBuf resetWriterIndex() {
        return this.buf.resetWriterIndex();
    }

    @Override
    public ByteBuf discardReadBytes() {
        return this.buf.discardReadBytes();
    }

    @Override
    public ByteBuf discardSomeReadBytes() {
        return this.buf.discardSomeReadBytes();
    }

    @Override
    public ByteBuf ensureWritable(int i) {
        return this.buf.ensureWritable(i);
    }

    @Override
    public int ensureWritable(int i, boolean flag) {
        return this.buf.ensureWritable(i, flag);
    }

    @Override
    public boolean getBoolean(int i) {
        return this.buf.getBoolean(i);
    }

    @Override
    public byte getByte(int i) {
        return this.buf.getByte(i);
    }

    @Override
    public short getUnsignedByte(int i) {
        return this.buf.getUnsignedByte(i);
    }

    @Override
    public short getShort(int i) {
        return this.buf.getShort(i);
    }

    @Override
    public int getUnsignedShort(int i) {
        return this.buf.getUnsignedShort(i);
    }

    @Override
    public int getMedium(int i) {
        return this.buf.getMedium(i);
    }

    @Override
    public int getUnsignedMedium(int i) {
        return this.buf.getUnsignedMedium(i);
    }

    @Override
    public int getInt(int i) {
        return this.buf.getInt(i);
    }

    @Override
    public long getUnsignedInt(int i) {
        return this.buf.getUnsignedInt(i);
    }

    @Override
    public long getLong(int i) {
        return this.buf.getLong(i);
    }

    @Override
    public char getChar(int i) {
        return this.buf.getChar(i);
    }

    @Override
    public float getFloat(int i) {
        return this.buf.getFloat(i);
    }

    @Override
    public double getDouble(int i) {
        return this.buf.getDouble(i);
    }

    @Override
    public ByteBuf getBytes(int i, ByteBuf bytebuf) {
        return this.buf.getBytes(i, bytebuf);
    }

    @Override
    public ByteBuf getBytes(int i, ByteBuf bytebuf, int j) {
        return this.buf.getBytes(i, bytebuf, j);
    }

    @Override
    public ByteBuf getBytes(int i, ByteBuf bytebuf, int j, int k) {
        return this.buf.getBytes(i, bytebuf, j, k);
    }

    @Override
    public ByteBuf getBytes(int i, byte abyte[]) {
        return this.buf.getBytes(i, abyte);
    }

    @Override
    public ByteBuf getBytes(int i, byte abyte[], int j, int k) {
        return this.buf.getBytes(i, abyte, j, k);
    }

    @Override
    public ByteBuf getBytes(int i, ByteBuffer bytebuffer) {
        return this.buf.getBytes(i, bytebuffer);
    }

    @Override
    public ByteBuf getBytes(int i, OutputStream outputstream, int j) throws IOException {
        return this.buf.getBytes(i, outputstream, j);
    }

    @Override
    public int getBytes(int i, GatheringByteChannel gatheringbytechannel, int j) throws IOException {
        return this.buf.getBytes(i, gatheringbytechannel, j);
    }

    @Override
    public ByteBuf setBoolean(int i, boolean flag) {
        return this.buf.setBoolean(i, flag);
    }

    @Override
    public ByteBuf setByte(int i, int j) {
        return this.buf.setByte(i, j);
    }

    @Override
    public ByteBuf setShort(int i, int j) {
        return this.buf.setShort(i, j);
    }

    @Override
    public ByteBuf setMedium(int i, int j) {
        return this.buf.setMedium(i, j);
    }

    @Override
    public ByteBuf setInt(int i, int j) {
        return this.buf.setInt(i, j);
    }

    @Override
    public ByteBuf setLong(int i, long j) {
        return this.buf.setLong(i, j);
    }

    @Override
    public ByteBuf setChar(int i, int j) {
        return this.buf.setChar(i, j);
    }

    @Override
    public ByteBuf setFloat(int i, float f) {
        return this.buf.setFloat(i, f);
    }

    @Override
    public ByteBuf setDouble(int i, double d0) {
        return this.buf.setDouble(i, d0);
    }

    @Override
    public ByteBuf setBytes(int i, ByteBuf bytebuf) {
        return this.buf.setBytes(i, bytebuf);
    }

    @Override
    public ByteBuf setBytes(int i, ByteBuf bytebuf, int j) {
        return this.buf.setBytes(i, bytebuf, j);
    }

    @Override
    public ByteBuf setBytes(int i, ByteBuf bytebuf, int j, int k) {
        return this.buf.setBytes(i, bytebuf, j, k);
    }

    @Override
    public ByteBuf setBytes(int i, byte abyte[]) {
        return this.buf.setBytes(i, abyte);
    }

    @Override
    public ByteBuf setBytes(int i, byte abyte[], int j, int k) {
        return this.buf.setBytes(i, abyte, j, k);
    }

    @Override
    public ByteBuf setBytes(int i, ByteBuffer bytebuffer) {
        return this.buf.setBytes(i, bytebuffer);
    }

    @Override
    public int setBytes(int i, InputStream inputstream, int j) throws IOException {
        return this.buf.setBytes(i, inputstream, j);
    }

    @Override
    public int setBytes(int i, ScatteringByteChannel scatteringbytechannel, int j) throws IOException {
        return this.buf.setBytes(i, scatteringbytechannel, j);
    }

    @Override
    public ByteBuf setZero(int i, int j) {
        return this.buf.setZero(i, j);
    }

    @Override
    public boolean readBoolean() {
        return this.buf.readBoolean();
    }

    @Override
    public byte readByte() {
        return this.buf.readByte();
    }

    @Override
    public short readUnsignedByte() {
        return this.buf.readUnsignedByte();
    }

    @Override
    public short readShort() {
        return this.buf.readShort();
    }

    @Override
    public int readUnsignedShort() {
        return this.buf.readUnsignedShort();
    }

    @Override
    public int readMedium() {
        return this.buf.readMedium();
    }

    @Override
    public int readUnsignedMedium() {
        return this.buf.readUnsignedMedium();
    }

    @Override
    public int readInt() {
        return this.buf.readInt();
    }

    @Override
    public long readUnsignedInt() {
        return this.buf.readUnsignedInt();
    }

    @Override
    public long readLong() {
        return this.buf.readLong();
    }

    @Override
    public char readChar() {
        return this.buf.readChar();
    }

    @Override
    public float readFloat() {
        return this.buf.readFloat();
    }

    @Override
    public double readDouble() {
        return this.buf.readDouble();
    }

    @Override
    public ByteBuf readBytes(int i) {
        return this.buf.readBytes(i);
    }

    @Override
    public ByteBuf readSlice(int i) {
        return this.buf.readSlice(i);
    }

    @Override
    public ByteBuf readBytes(ByteBuf bytebuf) {
        return this.buf.readBytes(bytebuf);
    }

    @Override
    public ByteBuf readBytes(ByteBuf bytebuf, int i) {
        return this.buf.readBytes(bytebuf, i);
    }

    @Override
    public ByteBuf readBytes(ByteBuf bytebuf, int i, int j) {
        return this.buf.readBytes(bytebuf, i, j);
    }

    @Override
    public ByteBuf readBytes(byte abyte[]) {
        return this.buf.readBytes(abyte);
    }

    @Override
    public ByteBuf readBytes(byte abyte[], int i, int j) {
        return this.buf.readBytes(abyte, i, j);
    }

    @Override
    public ByteBuf readBytes(ByteBuffer bytebuffer) {
        return this.buf.readBytes(bytebuffer);
    }

    @Override
    public ByteBuf readBytes(OutputStream outputstream, int i) throws IOException {
        return this.buf.readBytes(outputstream, i);
    }

    @Override
    public int readBytes(GatheringByteChannel gatheringbytechannel, int i) throws IOException {
        return this.buf.readBytes(gatheringbytechannel, i);
    }

    public String readString(){
        int index = readVariableInt();
        byte[] bytes = new byte[index];
        readBytes(bytes);
        return new String(bytes, Charset.forName("UTF-8"));
    }

    public int readVariableInt(){
        int out = 0, bytes = 0;
        byte part;
        do {
            part = this.buf.readByte();
            out |= (part & 0x7F) << (bytes++ * 7);
            if(bytes > 5) {
                throw new DecoderException(String.format("Variable Integer is too long (%d > 5)",bytes));
            }
        } while((part & 0x80) == 0x80);
        return out;
    }

    public UUID readUUID(){
        return new UUID(readLong(),readLong());
    }

    public <E> E readEnum(Class<E> enumClass){
        return enumClass.getEnumConstants()[readInt()];
    }

    @Override
    public ByteBuf skipBytes(int i) {
        return this.buf.skipBytes(i);
    }

    @Override
    public ByteBuf writeBoolean(boolean flag) {
        return this.buf.writeBoolean(flag);
    }

    @Override
    public ByteBuf writeByte(int i) {
        return this.buf.writeByte(i);
    }

    @Override
    public ByteBuf writeShort(int i) {
        return this.buf.writeShort(i);
    }

    @Override
    public ByteBuf writeMedium(int i) {
        return this.buf.writeMedium(i);
    }

    @Override
    public ByteBuf writeInt(int i) {
        return this.buf.writeInt(i);
    }

    @Override
    public ByteBuf writeLong(long i) {
        return this.buf.writeLong(i);
    }

    @Override
    public ByteBuf writeChar(int i) {
        return this.buf.writeChar(i);
    }

    @Override
    public ByteBuf writeFloat(float f) {
        return this.buf.writeFloat(f);
    }

    @Override
    public ByteBuf writeDouble(double d0) {
        return this.buf.writeDouble(d0);
    }

    @Override
    public ByteBuf writeBytes(ByteBuf bytebuf) {
        return this.buf.writeBytes(bytebuf);
    }

    @Override
    public ByteBuf writeBytes(ByteBuf bytebuf, int i) {
        return this.buf.writeBytes(bytebuf, i);
    }

    @Override
    public ByteBuf writeBytes(ByteBuf bytebuf, int i, int j) {
        return this.buf.writeBytes(bytebuf, i, j);
    }

    @Override
    public ByteBuf writeBytes(byte abyte[]) {
        return this.buf.writeBytes(abyte);
    }

    @Override
    public ByteBuf writeBytes(byte abyte[], int i, int j) {
        return this.buf.writeBytes(abyte, i, j);
    }

    @Override
    public ByteBuf writeBytes(ByteBuffer bytebuffer) {
        return this.buf.writeBytes(bytebuffer);
    }

    @Override
    public int writeBytes(InputStream inputstream, int i) throws IOException {
        return this.buf.writeBytes(inputstream, i);
    }

    @Override
    public int writeBytes(ScatteringByteChannel scatteringbytechannel, int i) throws IOException {
        return this.buf.writeBytes(scatteringbytechannel, i);
    }

    @Override
    public ByteBuf writeZero(int i) {
        return this.buf.writeZero(i);
    }

    public void writeString(String str){
        writeVariableInt(str.length());
        writeBytes(str.getBytes(Charset.forName("UTF-8")));
    }

    public void writeVariableInt(int value){
        byte part;
        do {
            part = (byte) (value & 0x7F);
            value >>>= 7;
            if(value != 0)
                part |= 0x80;
            this.buf.writeByte(part);
        } while (value != 0);
    }

    public void writeUUID(UUID uuid){
        this.buf.writeLong(uuid.getMostSignificantBits());
        this.buf.writeLong(uuid.getLeastSignificantBits());
    }

    public void writeEnum(Enum<?> enumSource){
        writeInt(enumSource.ordinal());
    }

    @Override
    public int indexOf(int i, int j, byte b0) {
        return this.buf.indexOf(i, j, b0);
    }

    @Override
    public int bytesBefore(byte b0) {
        return this.buf.bytesBefore(b0);
    }

    @Override
    public int bytesBefore(int i, byte b0) {
        return this.buf.bytesBefore(i, b0);
    }

    @Override
    public int bytesBefore(int i, int j, byte b0) {
        return this.buf.bytesBefore(i, j, b0);
    }

    @Override
    public int forEachByte(ByteBufProcessor processor) {
        return this.buf.forEachByte(processor);
    }

    @Override
    public int forEachByte(int index, int length, ByteBufProcessor processor) {
        return this.buf.forEachByte(index,length,processor);
    }

    @Override
    public int forEachByteDesc(ByteBufProcessor processor) {
        return this.buf.forEachByteDesc(processor);
    }

    @Override
    public int forEachByteDesc(int index, int length, ByteBufProcessor processor) {
        return this.buf.forEachByteDesc(index,length,processor);
    }

    @Override
    public ByteBuf copy() {
        return this.buf.copy();
    }

    @Override
    public ByteBuf copy(int i, int j) {
        return this.buf.copy(i, j);
    }

    @Override
    public ByteBuf slice() {
        return this.buf.slice();
    }

    @Override
    public ByteBuf slice(int i, int j) {
        return this.buf.slice(i, j);
    }

    @Override
    public ByteBuf duplicate() {
        return this.buf.duplicate();
    }

    @Override
    public int nioBufferCount() {
        return this.buf.nioBufferCount();
    }

    @Override
    public ByteBuffer nioBuffer() {
        return this.buf.nioBuffer();
    }

    @Override
    public ByteBuffer nioBuffer(int i, int j) {
        return this.buf.nioBuffer(i, j);
    }

    @Override
    public ByteBuffer internalNioBuffer(int i, int j) {
        return this.buf.internalNioBuffer(i, j);
    }

    @Override
    public ByteBuffer[] nioBuffers() {
        return this.buf.nioBuffers();
    }

    @Override
    public ByteBuffer[] nioBuffers(int i, int j) {
        return this.buf.nioBuffers(i, j);
    }

    @Override
    public boolean hasArray() {
        return this.buf.hasArray();
    }

    @Override
    public byte[] array() {
        return this.buf.array();
    }

    @Override
    public int arrayOffset() {
        return this.buf.arrayOffset();
    }

    @Override
    public boolean hasMemoryAddress() {
        return this.buf.hasMemoryAddress();
    }

    @Override
    public long memoryAddress() {
        return this.buf.memoryAddress();
    }

    @Override
    public String toString(Charset charset) {
        return this.buf.toString(charset);
    }

    @Override
    public String toString(int i, int j, Charset charset) {
        return this.buf.toString(i, j, charset);
    }

    @Override
    public int hashCode() {
        return this.buf.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        return this.buf.equals(object);
    }

    @Override
    public int compareTo(ByteBuf bytebuf) {
        return this.buf.compareTo(bytebuf);
    }

    @Override
    public String toString() {
        return this.buf.toString();
    }

    @Override
    public ByteBuf retain(int i) {
        return this.buf.retain(i);
    }

    @Override
    public ByteBuf retain() {
        return this.buf.retain();
    }

    @Override
    public ByteBuf touch() {
        return this.buf.touch();
    }

    @Override
    public ByteBuf touch(Object hint) {
        return this.buf.touch(hint);
    }

    @Override
    public int refCnt() {
        return this.buf.refCnt();
    }

    @Override
    public boolean release() {
        return this.buf.release();
    }

    @Override
    public boolean release(int i) {
        return this.buf.release(i);
    }

}
