package de.chickenpat.patscloud.core.util.logmanager;

public enum LogColorType {

    RESET(0),
    BLACK(30), //1
    RED(31), //2
    GREEN(32), //3
    YELLOW(33), //4
    BLUE(34), //5
    PURPLE(35), //6
    CYAN(36), //7
    WHITE(37); //8

    private Integer id;

    LogColorType(int id) {
        this.id = id;
    }

    public Integer getID() {
        return id;
    }

    @Override
    public String toString() {
        return new LogColor(this).getColorStr();
    }
}
