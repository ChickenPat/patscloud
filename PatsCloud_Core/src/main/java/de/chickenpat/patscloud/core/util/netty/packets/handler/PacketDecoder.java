package de.chickenpat.patscloud.core.util.netty.packets.handler;

import de.chickenpat.patscloud.core.util.CloudHelper;
import de.chickenpat.patscloud.core.util.logmanager.LogManager;
import de.chickenpat.patscloud.core.util.netty.packets.Packet;
import de.chickenpat.patscloud.core.util.netty.packets.buffer.PatsBuffer;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;


import java.util.List;

public class PacketDecoder extends ReplayingDecoder {

    protected void decode(ChannelHandlerContext ctx, ByteBuf byteBuf, List<Object> output) throws Exception {
        PatsBuffer patsBuffer = new PatsBuffer(byteBuf);
        int id = patsBuffer.readVariableInt();
        Class<? extends Packet> packetClass = CloudHelper.getInstance().getPacketViaID(id);
        if (packetClass == null)
            throw new NullPointerException("Can't find packet by id " + id);

        Packet packet = packetClass.newInstance();
        packet.read(patsBuffer);
        output.add(packet);
        LogManager.getInstance().sendDebugMessage("Receive Packet » Identification: " + CloudHelper.getInstance().getIDViaPacket(packet.getClass()) + " Name: " + packet.getClass().getSimpleName());
    }
}
