package de.chickenpat.patscloud.core.util.types.wrapper;

import java.util.ArrayList;

public class Template {

    private String name, startCommand;
    private Integer maxPlayers, minRam, maxRam, minServer, maxServer, startup;
    private boolean maintenance;
    private ArrayList<Server> servers = new ArrayList<Server>();
    private ArrayList<Name> names = new ArrayList<Name>();


    public Template(String name, String startCommand, Integer maxPlayers, Integer minRam, Integer maxRam, Integer minServer, Integer maxServer, Boolean maintenance) {
        this.name = name;
        this.startCommand = startCommand;
        this.maxPlayers = maxPlayers;
        this.minRam = minRam;
        this.maxRam = maxRam;
        this.minServer = minServer;
        this.maxServer = maxServer;
        this.startup = 0;
        this.maintenance = maintenance;
    }

    public boolean isMaintenance() {
        return maintenance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartCommand() {
        return startCommand;
    }

    public void setStartCommand(String startCommand) {
        this.startCommand = startCommand;
    }

    public Integer getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(Integer maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public Integer getMinRam() {
        return minRam;
    }

    public void setMinRam(Integer minRam) {
        this.minRam = minRam;
    }

    public Integer getMaxRam() {
        return maxRam;
    }

    public void setMaxRam(Integer maxRam) {
        this.maxRam = maxRam;
    }

    public Integer getMinServer() {
        return minServer;
    }

    public void setMinServer(Integer minServer) {
        this.minServer = minServer;
    }

    public Integer getMaxServer() {
        return maxServer;
    }

    public void setMaxServer(Integer maxServer) {
        this.maxServer = maxServer;
    }

    public Integer getStartup() {
        return startup;
    }

    public void setStartup(Integer startup) {
        this.startup = startup;
    }

    public ArrayList<Server> getServers() {
        return servers;
    }

    public void setServers(ArrayList<Server> servers) {
        this.servers = servers;
    }

    public void addServer(Server server) {
        servers.add(server);
    }

    public void removeServer(Server server) {
        servers.remove(server);
    }

    public void addName(Name name) {
        names.add(name);
    }

    public Name getFreeName() {
        for (Name name : names) {
            if (!name.isUsed()) {
                return name;
            }
        }
        Name name = new Name(this.name + "-" + names.size() + 1, null, null, true);
        addName(name);
        return name;
    }

    public void removeName(String name) {
        for (Name n : names) {
            if (n.getName().equals(name)) {
                n.reset();
            }
        }
    }

    public void removeName(Name name) {
        for (Name n : names) {
            if (n.getName().equals(name.getName())) {
                n.reset();
            }
        }
    }
}
