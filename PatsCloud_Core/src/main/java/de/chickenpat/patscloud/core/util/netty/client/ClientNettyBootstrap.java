package de.chickenpat.patscloud.core.util.netty.client;

import de.chickenpat.patscloud.core.util.CoreSettings;
import de.chickenpat.patscloud.core.util.logmanager.LogManager;
import de.chickenpat.patscloud.core.util.logmanager.LogType;
import de.chickenpat.patscloud.core.util.netty.NettyConnection;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.util.Timer;
import java.util.TimerTask;

public class ClientNettyBootstrap implements NettyConnection {

    private Bootstrap bootstrap = new Bootstrap();
    private Channel channel;
    private Timer timer;
    private EventLoopGroup workerGroup;
    private String host;
    private Integer port;
    private Boolean epoll;
    private Boolean keepalive;

    private Boolean exit = false;

    public ClientNettyBootstrap(Integer port, Boolean epoll, Boolean keepalive, String HOST, Timer timer, ChannelInitializer<SocketChannel> channelInitializer) {
        this.host = HOST;
        this.timer = timer;
        this.epoll = epoll;
        this.port = port;
        this.keepalive = keepalive;
        workerGroup = epoll ? new EpollEventLoopGroup() : new NioEventLoopGroup();
        bootstrap.group(workerGroup)
        .channel(epoll ? EpollSocketChannel.class : NioSocketChannel.class)
        .option(ChannelOption.SO_KEEPALIVE, keepalive)
        .handler(channelInitializer);
        LogManager.getInstance().sendMessage("Attempt to connect to " + HOST + ":" + port + " ...");
        scheduleConnect(5000);
    }

    public ClientNettyBootstrap(String HOST, Integer PORT, Timer timer, ChannelInitializer<SocketChannel> channelInitializer) {
        this(PORT,CoreSettings.EPOLL,CoreSettings.KEEPALIVE,HOST,timer,channelInitializer);
    }

    public void close() {
        try {
            exit = true;
            timer.cancel();
            channel.close().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            workerGroup.shutdownGracefully();
        }
    }


    public void doConnect() {
        try {
            ChannelFuture f = bootstrap.connect(host, port).sync();

            f.addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    if (!future.isSuccess()) {
                        LogManager.getInstance().sendMessage(LogType.ERROR, "Can't connect to " + host + ":" + port);
                        future.channel().close();
                        LogManager.getInstance().sendMessage("Attempt to connect to " + host + ":" + port + " ...");
                        bootstrap.connect(host, port).sync().addListener(this);
                    } else {
                        channel = future.channel();
                        addCloseDetectListener(channel);
                    }
                }

                private void addCloseDetectListener(Channel channel) {
                    channel.closeFuture().addListener((ChannelFutureListener) future -> {
                        if (!exit) {
                            LogManager.getInstance().sendMessage(LogType.ERROR, "Connection Lost");
                            scheduleConnect(5);
                        }
                    });

                }
            });
        } catch (Exception ex) {
            scheduleConnect(5000);
        }
    }

    private void scheduleConnect(long millis) {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                doConnect();
            }
        }, millis);
    }

    public Bootstrap getBootstrap() {
        return bootstrap;
    }

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public Timer getTimer() {
        return timer;
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }

    public EventLoopGroup getWorkerGroup() {
        return workerGroup;
    }

    public void setWorkerGroup(EventLoopGroup workerGroup) {
        this.workerGroup = workerGroup;
    }

    public String getHost() { return host; }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Boolean getEpoll() {
        return epoll;
    }

    public void setEpoll(Boolean epoll) {
        this.epoll = epoll;
    }

    public Boolean getKeepalive() {
        return keepalive;
    }

    public void setKeepalive(Boolean keepalive) {
        this.keepalive = keepalive;
    }

    public Boolean getExit() {
        return exit;
    }

    public void setExit(Boolean exit) {
        this.exit = exit;
    }
}
