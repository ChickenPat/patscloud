package de.chickenpat.patscloud.core.util.json;


import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;

public class JSONConfiguration {

    private JSONParser parser = new JSONParser();
    private JSONObject jsonObject;

    public JSONConfiguration(File file) throws FileNotFoundException, UnsupportedEncodingException {
        this(new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8")));
    }

    public JSONConfiguration(InputStreamReader reader) {
        this(new BufferedReader(reader));
    }

    public JSONConfiguration(FileInputStream stream) throws UnsupportedEncodingException {
        this(new BufferedReader(new InputStreamReader(stream, "UTF-8")));
    }

    public JSONConfiguration(BufferedReader reader) {
        try {
            Object obj = parser.parse(reader);
            jsonObject = (JSONObject) obj;
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    public JSONConfiguration(String jsonText) {
        try {
            Object obj = parser.parse(jsonText);
            jsonObject = (JSONObject) obj;
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }
}
