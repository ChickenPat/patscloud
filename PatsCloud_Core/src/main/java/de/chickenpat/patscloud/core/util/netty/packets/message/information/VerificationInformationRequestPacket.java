/*
 * Copyright (c) 2018 by ChickenPat.
 */

package de.chickenpat.patscloud.core.util.netty.packets.message.information;

import de.chickenpat.patscloud.core.util.netty.packets.Packet;
import de.chickenpat.patscloud.core.util.netty.packets.buffer.PatsBuffer;

public class VerificationInformationRequestPacket extends Packet {

    public VerificationInformationRequestPacket() { }

    @Override
    public void write(PatsBuffer byteBuf) {

    }

    @Override
    public void read(PatsBuffer byteBuf) {

    }
}