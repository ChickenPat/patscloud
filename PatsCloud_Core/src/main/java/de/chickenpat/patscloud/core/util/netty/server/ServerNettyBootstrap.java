package de.chickenpat.patscloud.core.util.netty.server;

import de.chickenpat.patscloud.core.util.CoreSettings;
import de.chickenpat.patscloud.core.util.logmanager.LogManager;
import de.chickenpat.patscloud.core.util.logmanager.LogType;
import de.chickenpat.patscloud.core.util.netty.NettyConnection;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class ServerNettyBootstrap implements NettyConnection {

    private ServerBootstrap bootstrap = new ServerBootstrap();
    private Channel channel;
    private EventLoopGroup workerGroup, bossGroup;
    private Integer port;
    private Integer backlog;
    private Boolean epoll;
    private Boolean keepalive;

    public ServerNettyBootstrap(Integer port, Integer backlog, Boolean epoll, Boolean keepalive, ChannelInitializer<SocketChannel> channelInitializer) {
        this.epoll = epoll;
        this.port = port;
        this.backlog = backlog;
        this.keepalive = keepalive;
        workerGroup = epoll ? new EpollEventLoopGroup() : new NioEventLoopGroup();
        bossGroup = epoll ? new EpollEventLoopGroup() : new NioEventLoopGroup();
        bootstrap.group(bossGroup,workerGroup)
        .channel(epoll ? EpollServerSocketChannel.class : NioServerSocketChannel.class)
        .childHandler(channelInitializer)
        .childOption(ChannelOption.SO_KEEPALIVE,keepalive);
        doConnect();
    }

    public ServerNettyBootstrap(ChannelInitializer<SocketChannel> channelInitializer) {
        this(CoreSettings.PORT,CoreSettings.BACKLOG,CoreSettings.EPOLL,CoreSettings.KEEPALIVE,channelInitializer);
    }

    public void close() {
        try {
            channel.close().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }

    public void doConnect() {
        try {
            ChannelFuture f = bootstrap.bind(port).sync();
            LogManager.getInstance().sendMessage("Netty Server started » Port: " + port);
            channel = f.channel();

        } catch (InterruptedException e) {
            e.printStackTrace();
            LogManager.getInstance().sendMessage(LogType.ERROR, "Can't start Server!");
        }
    }

    public ServerBootstrap getBootstrap() {
        return bootstrap;
    }

    public void setBootstrap(ServerBootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public EventLoopGroup getWorkerGroup() {
        return workerGroup;
    }

    public void setWorkerGroup(EventLoopGroup workerGroup) {
        this.workerGroup = workerGroup;
    }

    public EventLoopGroup getBossGroup() {
        return bossGroup;
    }

    public void setBossGroup(EventLoopGroup bossGroup) {
        this.bossGroup = bossGroup;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Integer getBacklog() {
        return backlog;
    }

    public void setBacklog(Integer backlog) {
        this.backlog = backlog;
    }

    public Boolean getEpoll() {
        return epoll;
    }

    public void setEpoll(Boolean epoll) {
        this.epoll = epoll;
    }

    public Boolean getKeepalive() {
        return keepalive;
    }

    public void setKeepalive(Boolean keepalive) {
        this.keepalive = keepalive;
    }
}
