/*
 * Copyright (c) 2018 by ChickenPat.
 */

package de.chickenpat.patscloud.core.util.netty.packets.message.password;

import de.chickenpat.patscloud.core.util.netty.packets.Packet;
import de.chickenpat.patscloud.core.util.netty.packets.buffer.PatsBuffer;
import io.netty.buffer.ByteBuf;

public class PasswordDenyPacket extends Packet {

    public PasswordDenyPacket() {
    }

    @Override
    public void write(PatsBuffer byteBuf) {

    }

    @Override
    public void read(PatsBuffer byteBuf) {

    }
}