package de.chickenpat.patscloud.core.util.logmanager;


public class LogColor {


    private boolean background, highIntensity = false;
    private LogColorType logColor;
    private LogColorFormatting colorFormatting;

    public LogColor(LogColorType logColor) {
        this(logColor, LogColorFormatting.NONE);
    }

    public LogColor(LogColorType logColor, LogColorFormatting colorFormatting) {
        this.logColor = logColor;
        this.colorFormatting = colorFormatting;
    }

    public String getColorStr() {
        if (logColor == LogColorType.RESET) {
            return "\033[0m";
        }
        Integer id = this.logColor.getID();
        if (background) id += 10;
        if (highIntensity) id += 60;

        return "\033[" + colorFormatting.getVar() + ";" + id + "m";
    }

    public LogColor setHighintensity(boolean highIntensity) {
        this.highIntensity = highIntensity;
        return this;
    }

    public LogColor setBackground(boolean background) {
        this.background = background;
        return this;
    }


    @Override
    public String toString() {
        return getColorStr();
    }
}
