package de.chickenpat.patscloud.core.util.types.wrapper;

public enum ServerType {

    BUKKIT,
    SPIGOT,
    SPONGE,
    BUNGEECORD,
    VANILLA,
    CRAFTBUKKIT,
    FORGE,
    PAPERSPIGOT,
    TACOSPIGOT,
    GLOWSTONE,
    WATERFALL,
    HEXACORD;


}
