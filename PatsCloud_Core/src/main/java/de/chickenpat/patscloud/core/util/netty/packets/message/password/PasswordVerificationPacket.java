/*
 * Copyright (c) 2018 by ChickenPat.
 */

package de.chickenpat.patscloud.core.util.netty.packets.message.password;

import de.chickenpat.patscloud.core.util.netty.packets.Packet;
import de.chickenpat.patscloud.core.util.netty.packets.buffer.PatsBuffer;

public class PasswordVerificationPacket extends Packet {


    private String pwd;
    private Integer pwdPart;


    public PasswordVerificationPacket(){}

    public PasswordVerificationPacket(String password, Integer pwdPart){
        this.pwd = password;
        this.pwdPart = pwdPart;
    }

    @Override
    public void write(PatsBuffer byteBuf) {
        byteBuf.writeVariableInt(pwdPart);
        byteBuf.writeString(pwd);
    }

    @Override
    public void read(PatsBuffer byteBuf) {
        this.pwdPart = byteBuf.readVariableInt();
        this.pwd = byteBuf.readString();
    }

    public String getPassword() {
        return pwd;
    }

    public Integer getPasswordPart() {
        return pwdPart;
    }

}
