package de.chickenpat.patscloud.core.util;

import io.netty.channel.epoll.Epoll;

public interface CoreSettings {
    Integer BACKLOG = 50;
    Boolean EPOLL = Epoll.isAvailable();
    Boolean KEEPALIVE = true;
    Integer PORT = 38965;
}
