package de.chickenpat.patscloud.core.util;

import de.chickenpat.patscloud.core.util.netty.packets.Packet;
import de.chickenpat.patscloud.core.util.netty.packets.Protocol;
import de.chickenpat.patscloud.core.util.netty.packets.message.password.PasswordVerificationPacket;
import io.netty.channel.Channel;

import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.*;

public class CloudHelper {

    private static CloudHelper instance = new CloudHelper();

    public static CloudHelper getInstance() {
        return instance;
    }

    public String getCurrentTime() {
        Date now = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        return format.format(now);
    }

    public String getTime(long millis) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        return format.format(new Date(millis));
    }

    public String getCurrentTimeOfDay() {
        Date now = new Date();
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        return format.format(now);
    }

    public String getTimeOfDay(long millis) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        return format.format(new Date(millis));
    }

    public String getCurrentDate() {
        Date now = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        return format.format(now);
    }

    public String getDate(long millis) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        return format.format(new Date(millis));
    }

    public Class<? extends Packet> getPacketViaID(int packetID){
        for(Protocol protocol : Protocol.values()){
            if(protocol.getPacketID() == packetID){
                return protocol.getPacketClass();
            }
        }
        return null;
    }
    public int getIDViaPacket(Class<? extends Packet> packetClass){
        for(Protocol protocol : Protocol.values()){
            if(protocol.getPacketClass().equals(packetClass)){
                return protocol.getPacketID();
            }
        }
        return -1;
    }
    public byte[] strToByteArray(String s){
        return s.getBytes(Charset.forName("UTF-8"));
    }

    public String composePassword(HashMap<Integer,String> pwdHashMap){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < pwdHashMap.size()-1; i++){
            if(pwdHashMap.get(i) == null)
                throw new NullPointerException("\nA piece of the password is missing. ID: "+i);
            stringBuilder.append(pwdHashMap.get(i));
        }
        return stringBuilder.toString();
    }

    public void sendPassword(Channel channel, String password){
        ArrayList<PasswordVerificationPacket> passwordVerificationPackets = new ArrayList<>();
        String temp;
        int j = password.length()/900;
        for(int i = 0; i <= j; i++){
            if(i == j) {
                temp = password.substring((i*900));
                passwordVerificationPackets.add(new PasswordVerificationPacket(temp,i));
            }else{
                temp = password.substring((i*900),(i*900)+900);
                passwordVerificationPackets.add(new PasswordVerificationPacket(temp,i));
            }
        }

        passwordVerificationPackets.add(new PasswordVerificationPacket("--FINISH--",-1));

        for(PasswordVerificationPacket passwordVerificationPacket : passwordVerificationPackets) {
                channel.writeAndFlush(passwordVerificationPacket);
        }
    }
}
