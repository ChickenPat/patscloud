package de.chickenpat.patscloud.core.util.netty.packets;

import de.chickenpat.patscloud.core.util.netty.packets.buffer.PatsBuffer;

public abstract class Packet {
    public abstract void write(PatsBuffer byteBuf);
    public abstract void read(PatsBuffer byteBuf);
}
