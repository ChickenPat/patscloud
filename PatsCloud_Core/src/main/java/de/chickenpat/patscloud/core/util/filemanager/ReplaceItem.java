/*
 * Copyright (c) 2018 by ChickenPat.
 */

package de.chickenpat.patscloud.core.util.filemanager;

public class ReplaceItem {

    public ReplaceItem(String var, String var2){
        this.var = var;
        this.var2 = var2;
    }

    public String getVar() {
        return var;
    }

    public String getVar2() {
        return var2;
    }

    public void setVar(String var) {
        this.var = var;
    }

    public void setVar2(String var2) {
        this.var2 = var2;
    }

    private String var, var2;

}
