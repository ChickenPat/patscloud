package de.chickenpat.patscloud.core.util.filemanager;


import de.chickenpat.patscloud.core.util.Cloud;
import de.chickenpat.patscloud.core.util.logmanager.LogManager;
import de.chickenpat.patscloud.core.util.logmanager.LogType;

import java.io.IOException;
import java.util.HashMap;

public class FileManager {

    private ResourceFileLoader resourceFileLoader;
    private Cloud cloud;

    public FileManager(Cloud cloud) {
        this.cloud = cloud;
        resourceFileLoader = new ResourceFileLoader(new HashMap<>(), cloud.getDataFolder());
    }

    public ResourceFileLoader getResourceFileLoader() {
        return resourceFileLoader;
    }

    public void loadResourceFiles() {
        cloud.initResourceFiles();
        try {
            resourceFileLoader.loadDir();
        } catch (IOException e) {
            LogManager.getInstance().sendMessage(LogType.ERROR, e.getMessage());
        }
    }
}
