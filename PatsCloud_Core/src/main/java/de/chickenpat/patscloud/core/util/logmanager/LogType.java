package de.chickenpat.patscloud.core.util.logmanager;

public enum LogType {

    INFO("INFO", ""),
    ERROR(LogColorType.RED + "ERROR", "", new LogColor(LogColorType.RED)),
    WARNING(LogColorType.YELLOW + "WARNING", "", new LogColor(LogColorType.YELLOW));

    String prefix, suffix;
    LogColor prefixColor, color;

    LogType(String prefix, String suffix) {
        this(prefix, suffix, new LogColor(LogColorType.RESET));

    }

    LogType(String prefix, String suffix, LogColor prefixColor) {
        this(prefix, suffix, prefixColor, new LogColor(LogColorType.RESET));
    }

    LogType(String prefix, String suffix, LogColor prefixColor, LogColor color) {
        this.prefix = prefix;
        this.suffix = suffix;
        this.prefixColor = prefixColor;
        this.color = color;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public LogColor getPrefixColor() {
        return prefixColor;
    }

    public LogColor getColor() {
        return color;
    }

    @Override
    public String toString() {
        return super.toString();
    }

}
