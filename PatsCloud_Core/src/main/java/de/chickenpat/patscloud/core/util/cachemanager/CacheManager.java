/*
 * Copyright (c) 2018 by ChickenPat.
 */

package de.chickenpat.patscloud.core.util.cachemanager;

import java.util.HashMap;

public class CacheManager {

    private HashMap<String, Object> hashMap;

    public CacheManager(){
        hashMap = new HashMap<>();
    }

    public void put(String key, Object o){
        hashMap.put(key,o);
    }

    public void clear(){
        hashMap = new HashMap<>();
    }

    public void remove(String o){
        hashMap.remove(o);
    }

    public Integer getInteger(String key){
        return Integer.valueOf((String) get(key));
    }

    public Boolean getBoolean(String key){
        return Boolean.valueOf(String.valueOf(get(key)));
    }

    public Long getLong(String key){
        return Long.valueOf((String) get(key));
    }

    public Byte getByte(String key){
        return Byte.valueOf((String) get(key));
    }

    public Short getShort(String key){
        return Short.valueOf((String) get(key));
    }

    public Float getFlout(String key){
        return Float.valueOf((String) get(key));
    }

    public Double getDouble(String key){
        return Double.valueOf((String) get(key));
    }

    public Character getChar(String key){
        return (Character) get(key);
    }

    public String getString(String key){
        return (String) get(key);
    }

    public Object get(String key){
        return hashMap.get(key);
    }

    public HashMap<String, Object> getHashMap() {
        return hashMap;
    }
}
