package de.chickenpat.patscloud.core.util.types.wrapper;

public class Name {

    private String name;
    private Server server;
    private Wrapper wrapper;
    private Boolean used;

    public Name(String name, Server server, Wrapper wrapper, boolean used) {
        this.name = name;
        this.server = server;
        this.wrapper = wrapper;
        this.used = used;
    }

    public boolean isUsed() {
        return this.used;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Server getServer() {
        return server;
    }

    public void setServer(Server server) {
        this.server = server;
    }

    public Wrapper getWrapper() {
        return wrapper;
    }

    public void setWrapper(Wrapper wrapper) {
        this.wrapper = wrapper;
    }

    public void reset() {
        this.used = false;
        this.wrapper = null;
        this.server = null;
    }
}
