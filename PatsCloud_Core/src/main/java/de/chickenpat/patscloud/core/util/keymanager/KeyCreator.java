/*
 * Copyright (c) 2018 by ChickenPat.
 */

package de.chickenpat.patscloud.core.util.keymanager;

import de.chickenpat.patscloud.core.util.CoreSettings;
import de.chickenpat.patscloud.core.util.RandomString;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;


public class KeyCreator {

    private String raw, password, rawPassword;
    private File file;

    public KeyCreator(String fileName){
        this.file = new File(fileName);
        if(file.exists()){
            try {
                KeyLoader keyLoader = new KeyLoader(file);
                raw = keyLoader.getRaw();
                password = keyLoader.getPassword();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            this.rawPassword = new RandomString(300).nextString();
            transformToRaw();
            this.password = raw;
            integrateDates();
            createFile();
        }
    }

    private String asciiToHex(String asciiStr) {
        char[] chars = asciiStr.toCharArray();
        StringBuilder hex = new StringBuilder();
        for (char ch : chars) {
            hex.append(Integer.toHexString((int) ch));
        }

        return hex.toString();
    }
    public void integrateDates(){
        try {
            raw = raw + asciiToHex(InetAddress.getLocalHost().getHostAddress() + ";" + CoreSettings.PORT);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public void createFile(){
        try {
            FileUtils.writeStringToFile(file, raw);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void transformToRaw(){
        StringBuilder s = new StringBuilder();
        for(Character c : rawPassword.toCharArray()){
            s.append(MD5(String.valueOf(c)));
        }
        this.raw = s.toString();
    }



    public String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException ignored) { }
        return null;
    }

    public String getPassword(){
        return password;
    }

    public String getRaw() {
        return raw;
    }
}
