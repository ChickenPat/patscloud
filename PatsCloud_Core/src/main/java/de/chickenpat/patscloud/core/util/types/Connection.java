/*
 * Copyright (c) 2018 by ChickenPat.
 */

package de.chickenpat.patscloud.core.util.types;


public abstract class Connection {

    public String name, address;
    public Integer ram,usedRam;

    public abstract String getName();
    public abstract void setName(String name);

    public abstract String getAddress();
    public abstract void setAddress(String address);

    public abstract Integer getRam();
    public abstract void setRam(Integer ram);

    public abstract Integer getUsedRam();
    public abstract void setUsedRam(Integer ram);
}
