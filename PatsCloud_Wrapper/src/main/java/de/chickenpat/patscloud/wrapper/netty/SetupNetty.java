package de.chickenpat.patscloud.wrapper.netty;

import de.chickenpat.patscloud.core.util.Setup;
import de.chickenpat.patscloud.core.util.cachemanager.CacheManager;
import de.chickenpat.patscloud.core.util.command.CommandReader;
import de.chickenpat.patscloud.core.util.netty.client.ClientNettyBootstrap;
import de.chickenpat.patscloud.core.util.netty.packets.handler.PacketDecoder;
import de.chickenpat.patscloud.core.util.netty.packets.handler.PacketEncoder;
import de.chickenpat.patscloud.core.util.netty.server.ServerNettyBootstrap;
import de.chickenpat.patscloud.wrapper.PatsCloud_Wrapper;
import de.chickenpat.patscloud.wrapper.util.Settings;
import de.chickenpat.patscloud.wrapper.util.netty.handler.client.ConnectionHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Timer;


public class SetupNetty extends Setup implements Settings {

    private static SetupNetty instance = new SetupNetty();
    private BufferedReader bufferedReader;
    private ClientNettyBootstrap clientNettyBootstrap;
    private ServerNettyBootstrap serverNettyBootstrap;
    private CommandReader commandReader;

    public static SetupNetty getInstance() {
        return instance;
    }

    public CommandReader getCommandReader() {
        return commandReader;
    }

    public ClientNettyBootstrap getClientNettyBootstrap() {
        return clientNettyBootstrap;
    }

    public ServerNettyBootstrap getServerNettyBootstrap() {
        return serverNettyBootstrap;
    }

    public BufferedReader getBufferedReader() {
        return bufferedReader;
    }

    public void setBufferedReader(BufferedReader bufferedReader) {
        this.bufferedReader = bufferedReader;
    }

    @Override
    public void onLoad() {
    }

    @Override
    public void onEnable() {

        registerClientNettyBootstrap();
//        registerServerNettyBootstrap();

        commandReader = new CommandReader(PatsCloud_Wrapper.getWrapper(), clientNettyBootstrap);
        try {
            commandReader.startReader();
        } catch (IOException e) {
            commandReader.close();
        }

    }

    @Override
    public void onDisable() {
    }

    private void registerServerNettyBootstrap() {
        CacheManager cacheManager = PatsCloud_Wrapper.getWrapper().getCacheManager();
        serverNettyBootstrap = new ServerNettyBootstrap(cacheManager.getInteger("Port"),cacheManager.getInteger("BackLog"),cacheManager.getBoolean("Epoll"),cacheManager.getBoolean("KeepAlive"),getChannelChannelInitializer());

    }
    private void registerClientNettyBootstrap() {
        //Integer port, Boolean epoll, Boolean keepalive, String HOST, Timer timer, ChannelInitializer<?> channelInitializer
        CacheManager cacheManager = PatsCloud_Wrapper.getWrapper().getCacheManager();
        clientNettyBootstrap = new ClientNettyBootstrap(cacheManager.getInteger("Port"),cacheManager.getBoolean("Epoll"),cacheManager.getBoolean("KeepAlive"),cacheManager.getString("Host"), new Timer(), getChannelChannelInitializer());

    }

    private ChannelInitializer<SocketChannel> getChannelChannelInitializer() {
        return new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel channel) throws Exception {
                channel.pipeline().addLast(new PacketEncoder(),new PacketDecoder(),new ConnectionHandler());
            }
        };
    }

}
