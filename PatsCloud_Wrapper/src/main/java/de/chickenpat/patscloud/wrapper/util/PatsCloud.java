package de.chickenpat.patscloud.wrapper.util;

import de.chickenpat.patscloud.core.util.Cloud;
import de.chickenpat.patscloud.core.util.filemanager.FileManager;
import de.chickenpat.patscloud.core.util.filemanager.ReplaceItem;
import de.chickenpat.patscloud.core.util.json.JSONConfiguration;
import de.chickenpat.patscloud.core.util.keymanager.KeyLoader;
import de.chickenpat.patscloud.wrapper.PatsCloud_Wrapper;
import de.chickenpat.patscloud.wrapper.commands.Command_Exit;
import de.chickenpat.patscloud.wrapper.commands.Command_Info;
import de.chickenpat.patscloud.wrapper.resourcefile.actions.Config_FileCreateAction;
import io.netty.channel.epoll.Epoll;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class PatsCloud extends Cloud {

    public void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public File getDataFolder() {
        File file = new File("PatsCloud-Wrapper");
        if (!file.exists()) {
            file.mkdir();
        }
        return file;
    }

    @Override
    public void loadConfigFiles() {
        try {
            JSONConfiguration jsonConfiguration = new JSONConfiguration(new File(getDataFolder(),"config.json"));
            getCacheManager().put("Port",jsonConfiguration.getJsonObject().get("cloudPort"));
            getCacheManager().put("Host",jsonConfiguration.getJsonObject().get("cloudHost"));
            getCacheManager().put("Wrapper-id",jsonConfiguration.getJsonObject().get("wrapper-id"));
            getCacheManager().put("internal_ip",jsonConfiguration.getJsonObject().get("internal_ip"));
            getCacheManager().put("KeepAlive",jsonConfiguration.getJsonObject().get("keepAlive"));
            getCacheManager().put("BackLog",jsonConfiguration.getJsonObject().get("backLog"));
            getCacheManager().put("Epoll",((String)jsonConfiguration.getJsonObject().get("epoll")).equalsIgnoreCase("auto") ? Epoll.isAvailable() : jsonConfiguration.getJsonObject().get("epoll"));
            getCacheManager().put("startPort",jsonConfiguration.getJsonObject().get("startPort"));
            getCacheManager().put("max-memory",jsonConfiguration.getJsonObject().get("max-memory"));
            getCacheManager().put("backLog",jsonConfiguration.getJsonObject().get("backLog"));
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initResourceFiles() {
        FileManager fileManager = PatsCloud_Wrapper.getWrapper().getFileManager();
        KeyLoader keyLoader = PatsCloud_Wrapper.getWrapper().getKeyLoader();
        try {
            fileManager.getResourceFileLoader().registerResourceFile("config.json", "default", fileManager.getResourceFileLoader().createReplaceHashMap(new ReplaceItem("${WrapperHost}",keyLoader.getIp()),new ReplaceItem("${WrapperPort}",String.valueOf(keyLoader.getPort())),new ReplaceItem("${InternalIP}", InetAddress.getLocalHost().getHostAddress() == null ? "127.0.0.1": InetAddress.getLocalHost().getHostAddress())),new Config_FileCreateAction());

            getCacheManager().put("local_internal_ip",InetAddress.getLocalHost().getHostAddress() == null ? "127.0.0.1": InetAddress.getLocalHost().getHostAddress());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        try {
            fileManager.getResourceFileLoader().loadDir();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void registerCommands() {
        registerCommand("exit", new Command_Exit());
        registerCommand("info", new Command_Info());
    }

}
