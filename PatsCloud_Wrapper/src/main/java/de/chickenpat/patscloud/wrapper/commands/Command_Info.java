package de.chickenpat.patscloud.wrapper.commands;

import de.chickenpat.patscloud.core.util.command.Command;
import de.chickenpat.patscloud.core.util.logmanager.LogManager;
import io.netty.channel.Channel;

public class Command_Info implements Command {
    @Override
    public boolean called(String[] args, Channel channel) {
        return false;
    }

    @Override
    public void action(String[] args, Channel channel) {
        LogManager.getInstance().sendMessage(LogManager.getInstance().translateColorCodes('§', "§2§b✘§r »-=-=-=-=-=-=-«§b §r§7Information §r»-=-=-=-=-=-=-« §2§b✘"));


        LogManager.getInstance().sendMessage(LogManager.getInstance().translateColorCodes('§', "§2§b✘§r »-=-=-=-=-=-=-=-=-=« §2§b✘ §r»=-=-=-=-=-=-=-=-=-« §2§b✘"));

    }

    @Override
    public void executed(boolean success, Channel channel) {

    }
}
