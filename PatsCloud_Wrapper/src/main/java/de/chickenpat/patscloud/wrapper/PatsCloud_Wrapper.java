package de.chickenpat.patscloud.wrapper;


import de.chickenpat.patscloud.core.util.filemanager.FileManager;
import de.chickenpat.patscloud.core.util.keymanager.KeyLoader;
import de.chickenpat.patscloud.core.util.logmanager.LogManager;
import de.chickenpat.patscloud.core.util.logmanager.LogType;
import de.chickenpat.patscloud.wrapper.netty.SetupNetty;
import de.chickenpat.patscloud.wrapper.util.PatsCloud;
import de.chickenpat.patscloud.wrapper.util.Settings;

import java.io.File;

public class PatsCloud_Wrapper extends PatsCloud implements Settings {

    private static PatsCloud_Wrapper wrapper = new PatsCloud_Wrapper();
    private FileManager fileManager;
    private KeyLoader keyLoader;

    public static PatsCloud_Wrapper getWrapper() {
        return wrapper;
    }

    public static void main(String[] args) {
        //TEMP
        LogManager.getInstance().setDebug(true);
        //END:TEMP

        wrapper.sendStartUpMessage();
        wrapper.startUp();
    }

    public FileManager getFileManager() {
        return fileManager;
    }

    private void startUp() {
        if(!new File("WrapperKey.key").exists()) {
            LogManager.getInstance().sendMessage(LogType.ERROR, "The WrapperKey was not found. Please drag the key embarrassed by the master into the folder of the wrapper.");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ignored) { }
            return;
        }
        try {
            keyLoader = new KeyLoader(new File("WrapperKey.key"));
            passwort = keyLoader.getPassword();
        }catch(Exception ignored){ }

        LogManager.getInstance().sendMessage(LogType.INFO, "Initialize Wrapper Default Files...");
        fileManager = new FileManager(this);
        fileManager.loadResourceFiles();
        LogManager.getInstance().sendMessage(LogType.INFO, "Load Wrapper Configuration...");
        loadConfigFiles();
        LogManager.getInstance().sendMessage(LogType.INFO, "Initialize Wrapper Commands...");
        registerCommands();

        SetupNetty.getInstance().onEnable();
    }


    private void sendStartUpMessage() {
        System.out.println(
                "   ______       _          _____  _                    _ \n" +
                        "   | ___ \\     | |        /  __ \\| |                  | |\n" +
                        "   | |_/ /__ _ | |_  ___  | /  \\/| |  ___   _   _   __| |\n" +
                        "   |  __// _` || __|/ __| | |    | | / _ \\ | | | | / _` |\n" +
                        "   | |  | (_| || |_ \\__ \\ | \\__/\\| || (_) || |_| || (_| |\n" +
                        "   \\_|   \\__,_| \\__||___/  \\____/|_| \\___/  \\__,_| \\__,_|\n" +
                        "   Software: " + SOFTWARE + "    |   Version: " + VERSION + "\n");

    }

    public KeyLoader getKeyLoader() {
        return keyLoader;
    }
}
