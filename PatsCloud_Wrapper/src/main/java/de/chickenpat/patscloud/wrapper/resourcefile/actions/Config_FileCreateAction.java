/*
 * Copyright (c) 2018 by ChickenPat.
 */

package de.chickenpat.patscloud.wrapper.resourcefile.actions;

import de.chickenpat.patscloud.core.util.filemanager.ResourceCreateAction;
import de.chickenpat.patscloud.core.util.filemanager.ResourceFile;
import de.chickenpat.patscloud.core.util.logmanager.LogManager;
import de.chickenpat.patscloud.core.util.logmanager.LogType;

import java.util.Scanner;

public class Config_FileCreateAction extends ResourceCreateAction {

    @Override
    public void onPreCreation(ResourceFile file) {
        LogManager logManager = LogManager.getInstance();
        Scanner scanner = new Scanner(System.in);
        logManager.sendMessage(LogType.WARNING, "What is the Name of this Wrapper?");
        file.getReplace().put("${WrapperID}",scanner.nextLine());
        logManager.sendMessage(LogType.WARNING, "What is the Internal IP of the System? Auto or Manual(\"127.0.0.1\")");
        String iip = scanner.nextLine();
        if(!iip.equalsIgnoreCase("auto")){
            file.getReplace().put("${InternalIP}",iip);
        }
        logManager.sendMessage(LogType.WARNING, "How Many Server should this wrapper manage? (Default: 128)");
        file.getReplace().put("${BackLog}",scanner.nextLine());
        logManager.sendMessage(LogType.WARNING, "From which port should the servers be started? (Default: 40000)");
        file.getReplace().put("${startPort}",scanner.nextLine());
    }

    @Override
    public void onCreation(ResourceFile file) {
        LogManager logManager = LogManager.getInstance();
        logManager.sendMessage( "Wrapper successfully configured.");
    }
}
