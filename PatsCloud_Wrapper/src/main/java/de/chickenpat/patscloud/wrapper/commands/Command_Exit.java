package de.chickenpat.patscloud.wrapper.commands;

import de.chickenpat.patscloud.core.util.command.Command;
import de.chickenpat.patscloud.core.util.logmanager.LogManager;
import de.chickenpat.patscloud.wrapper.PatsCloud_Wrapper;
import de.chickenpat.patscloud.wrapper.netty.SetupNetty;
import io.netty.channel.Channel;

import java.util.Random;

public class Command_Exit implements Command {
    @Override
    public boolean called(String[] args, Channel channel) {
        return false;
    }

    @Override
    public void action(String[] args, Channel channel) {
        Random random = new Random();
        LogManager.getInstance().sendMessage("Wrapper is shutting down...");
        PatsCloud_Wrapper.getWrapper().sleep(random.nextInt(300) + 200);
        SetupNetty.getInstance().getClientNettyBootstrap().close();
        LogManager.getInstance().sendMessage("Netty wrapper has been closed");
        PatsCloud_Wrapper.getWrapper().sleep(random.nextInt(300) + 200);
        SetupNetty.getInstance().getCommandReader().close();
        LogManager.getInstance().sendMessage("Command Reader has been closed");
        PatsCloud_Wrapper.getWrapper().sleep(random.nextInt(300) + 200);
        LogManager.getInstance().sendMessage("Server shuts down successfully. Bye");
    }

    @Override
    public void executed(boolean success, Channel channel) {

    }
}
