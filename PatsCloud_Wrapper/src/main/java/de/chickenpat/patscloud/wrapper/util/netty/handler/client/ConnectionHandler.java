/*
 * Copyright (c) 2018 by ChickenPat.
 */

package de.chickenpat.patscloud.wrapper.util.netty.handler.client;

import de.chickenpat.patscloud.core.util.CloudHelper;
import de.chickenpat.patscloud.core.util.cachemanager.CacheManager;
import de.chickenpat.patscloud.core.util.logmanager.LogManager;
import de.chickenpat.patscloud.core.util.netty.packets.Packet;
import de.chickenpat.patscloud.core.util.netty.packets.message.information.VerificationInformationPacket;
import de.chickenpat.patscloud.core.util.netty.packets.message.information.VerificationInformationRequestPacket;
import de.chickenpat.patscloud.wrapper.PatsCloud_Wrapper;
import de.chickenpat.patscloud.wrapper.util.Settings;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class ConnectionHandler extends SimpleChannelInboundHandler<Packet> implements Settings {
    private Channel channel;

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        channel = ctx.channel();
        LogManager.getInstance().sendMessage("Successfully connected to " + PatsCloud_Wrapper.getWrapper().getCacheManager().getString("Host") + ":" + PORT);
        CloudHelper.getInstance().sendPassword(getChannel(), PatsCloud_Wrapper.getWrapper().passwort);
    }

    @Override
    protected void messageReceived(ChannelHandlerContext ctx, Packet packet) throws Exception {
        CacheManager cacheManager = PatsCloud_Wrapper.getWrapper().getCacheManager();
        LogManager.getInstance().sendDebugMessage("Receive Packet » Identification: " + CloudHelper.getInstance().getIDViaPacket(packet.getClass()) + " Name: " + packet.getClass().getSimpleName());
        if(packet instanceof VerificationInformationRequestPacket){
            channel.writeAndFlush(new VerificationInformationPacket(cacheManager.getString("Wrapper-id"),cacheManager.getString("local_internal_ip"),cacheManager.getInteger("max-memory"),cacheManager.getInteger("startPort"),cacheManager.getInteger("backLog")));
        }
    }

    public Channel getChannel() {
        return channel;
    }
}
